process.stdin.setEncoding('utf8');
const net = require('net');
const decoder = require('./decoder');

const password = '123456';

process.stdin.on('readable', () => {

    const decoding = new decoder(password);

    let recipient = -1;

    if(process.argv[2] !== undefined) {
        recipient = process.argv[2];
    }

    let input = process.stdin.read().trim();

    if (input === null) {
        return;
    }

    const client = net.createConnection({ port: 8124 }, () => {
        let time = new Date();

        let request = {
            clientId: "client app 1.0",
            recipientId: recipient,
            message: input,
            time: time.toJSON()
        };

        let json = JSON.stringify(request);
        let cryptMessage = decoding.cipheriv(json);

        client.write(cryptMessage);
    });

    client.on('data', () => {
        client.end();
    });
});