const net = require('net');
const decoder = require('./decoder');

const password = '123456';

const server = net.createServer((sock) => {

    const decoding = new decoder(password);

    sock.on('data', (chunk) => {

        let encryptData = decoding.decipheriv(chunk.toString());
        let data = JSON.parse(encryptData);
        console.log(data);
    });

    sock.pipe(sock);
});

server.on('error', (err) => {
    throw err;
});

server.listen(8124, () => {});