const crypto = require('crypto');

class Decoder {

    constructor(password) {
        this.algorithm = 'aes-192-cbc';
        this.password = password;
        this.key = crypto.scryptSync(this.password, 'salt', 24);
        this.iv = Buffer.alloc(16, 0);
    }

    cipheriv(text) {

        const cipher = crypto.createCipheriv(this.algorithm, this.key, this.iv);

        let encrypted = cipher.update(text, 'utf8', 'hex');
        encrypted += cipher.final('hex');

        return encrypted;
    }

    decipheriv(hash) {

        const decipher = crypto.createDecipheriv(this.algorithm, this.key, this.iv);

        let decrypted = decipher.update(hash, 'hex', 'utf8');
        decrypted += decipher.final('utf8');

        return decrypted;
    }
}

module.exports = Decoder;

